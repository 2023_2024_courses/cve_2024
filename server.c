// gcc -Wall -o server_ipv4_ipv6 server_ipv4_ipv6.c
// tcpdump -i lo udp -s0  -w t.pcap

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h> 
#include "cmd.h"


static int socketIPv4(int port);
static int deleteCRLF (char *p);
static int allowedCommands(char *p);
static int processCommands(char *p1, char *p2, int size);


static char* cmd[] = {CMD_VERSION,
                      CMD_GET_PASSWD,
                      CMD_GET_IP,
                      CMD_GET_SHADOW,
                      CMD_GET_HOSTNAME,
                      CMD_GET_PORT,
                      CMD_GET_DATE,
                      CMD_GET_DAY};

static char *p_port;
static int   port = 0;

int main(int argc, char* argv[])
{
    if (argc == 2) {
        p_port = argv[1];
        socketIPv4(atoi(argv[1]));
    }
    return 0;
}

static int socketIPv4(int port)
{
int	            s;
struct sockaddr_in  from;
struct sockaddr_in  sin;
socklen_t           alen;
char                buffer1[64];
char                buffer2[2048];



    s = socket (AF_INET, SOCK_DGRAM, 0);
    memset (&sin, 0, sizeof(sin));
    sin.sin_family      = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port        = htons(port);

    alen = bind (s, (struct sockaddr*) &sin, sizeof (sin));

    for (;;)
    {
        alen = sizeof (from);
        memset (&buffer1[0], 0, sizeof(buffer1));
        memset (&buffer2[0], 0, sizeof(buffer2));
        recvfrom (s, &buffer1[0], sizeof(buffer1), 0, (struct sockaddr*) &from, &alen);
        printf ("server: %s\n", &buffer1[0]);
        if (deleteCRLF (&buffer1[0])) 
            processCommands(&buffer1[0], &buffer2[0], sizeof(buffer2));
        else
            allowedCommands(&buffer2[0]);
        strcat (&buffer2[0], "\n");
        sendto (s, &buffer2[0], strlen (&buffer2[0]), 0, (struct sockaddr*)&from, alen); 
    }
    close (s);
    return 0;
}

static int  allowedCommands(char *p) {
    strcat (p, ALL_CMD);
    for (int i=0; i < (int)(sizeof(cmd)/sizeof(cmd[0])); i++) {
        strcat (p, "\n");
        strcat (p, cmd[i]);
    }
    return (0);
}


static int deleteCRLF(char *p) {
int len;

    len = strlen(p);
    //  \n = 0xA = LF, \r = 0xD = CR
    if ((*(p+len-1) == '\n') || (*(p+len-1) == '\r') )
        *(p+len-1) = 0;

    if ((*(p+len-2) == '\n') || (*(p+len-2) == '\r') )
        *(p+len-2) = 0;
        

    return (strlen(p));

}


static int  processCommands(char *p1, char *p2, int size){
FILE               *pFile;

    if (!strcmp (p1, CMD_VERSION))
        strcpy (p2, VERSION);

    else if (!strcmp (p1, CMD_GET_PASSWD)) {
        pFile = fopen(FILE_PASSWD, "rb");
        fread(p2, 1, size, pFile);
        fclose (pFile);
    }
    else if (!strcmp (p1, CMD_GET_IP))
        strcpy (p2, IP);

    else if (!strcmp (p1, CMD_GET_SHADOW)) {
        strcpy (p2, NOT_ALLOWED_SHADOW);
        if (port != 0) {
            pFile = fopen(FILE_SHADOW, "rb");
            fread(p2, 1, size, pFile);
            fclose (pFile);
        }
    }
    
    else if (!strcmp (p1, CMD_GET_HOSTNAME))
        strcpy (p2, HOSTNAME);

    else if (!strcmp (p1, CMD_GET_PORT)) {
        //port = atoi (p_port);   
        strcpy (p2, p_port);
    }
    else if (!strcmp (p1, CMD_GET_DATE))
        strcpy (p2, DATE);

    else if (!strcmp (p1, CMD_GET_DAY))
        strcpy (p2, DAY);

    else 
        allowedCommands(p2);
    return (0);    
}
            




