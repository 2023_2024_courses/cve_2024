#define CMD_VERSION    "version"
#define VERSION        "Version 0.6"

#define CMD_GET_PASSWD "get /etc/passwd"
#define FILE_PASSWD    "passwd"

#define CMD_GET_IP     "get IP"
#define IP             "160.98.34.63"

#define CMD_GET_SHADOW "get /etc/shadow"
#define FILE_SHADOW    "shadow"
#define NOT_ALLOWED_SHADOW "You are not allowed to get shadow"

#define CMD_GET_HOSTNAME "get hostname"
#define HOSTNAME         "taupe.heia-fr.ch"

#define CMD_GET_PORT     "get port"

#define CMD_GET_DATE     "get date"
#define DATE             "6.5.2024" 

#define CMD_GET_DAY     "get day"
#define DAY             "Monday" 



#define ALL_CMD        "Allowed commands:"
